import { Flex, Text } from "@chakra-ui/react";

export function Card(props){
    return(
        <>
            <Flex alignItems="center"  justifyContent="space-between" mt='2rem' w='50%' >
                <Text>{props.name}</Text>
                <small>{props.time}</small>
            </Flex>
        </>
    );

}