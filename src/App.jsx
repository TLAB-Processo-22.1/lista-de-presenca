import { Card } from "./components/Card/index";
import { Heading, Flex, Input, Button } from "@chakra-ui/react";
import React, {useState} from 'react'

export function App() {
  //estado que armazena o conteudo digitado pelo usuario
  const [studentName, setStudentName] = useState();
  
  //estado responsvel por armazenar os estudantes da lista de presença
  const [students, setStudents] = useState([]);

  //função que 
  function handleAddStudent(){
    const newStudent = {
      name: studentName,
      time: new Date().toLocaleTimeString('pt-br' , {
        hour: '2-digit',
        minute: '2-digit',
        second: '2-digit'
      })
    };
    
    //substituindo estado
    setStudents(prevState => [...prevState, newStudent])
  }
  

  return (
    <>
      <Flex alignItems="center" justifyContent="center">
        <Heading mb='1rem'>
          Lista de presença

        </Heading>
      </Flex>

      <Flex alignItems="center" justifyContent="center" flexDirection='column'>
        <Input 
          w='50%' 
          placeholder='Digite seu nome...'
          onChange={e => setStudentName(e.target.value)}
        />

        <Button 
          w='50%' 
          mb='2rem'
          onClick={handleAddStudent}
        >
            Enviar
        </Button>

        {/*Cartoes dinamicos, pecorrer o vetor da lista de presença */}
       
        
        {/*quando colocamos um elemeto entre chaves no jsx é pq queromos usar uma variavel*/}
        
        { 
          students.map(student => 
          
            <Card 
              key={student.time}
              name={student.name} 
              time={student.time}
          />)
          
        }


      </Flex>

      
    </>
  );
}
